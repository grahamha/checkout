﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkout.Dto
{
    public class Basket
    {
        public int BasketId { get; set; }
        public List<Item> Items { get; set; }
        public decimal Total
        {
            get
            {
                return Items.Sum(i => i.Price);
            }
        }

    }
}
