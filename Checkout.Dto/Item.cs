﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkout.Dto
{
    public class Item
    {
        public int ItemId { get; set; }
        public int BasketId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal Price
        {
            get
            {
                return Product.UnitPrice * Quantity;
            }
        }
    }
}
