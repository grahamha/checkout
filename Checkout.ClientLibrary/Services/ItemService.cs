﻿using Checkout.ClientLibrary.Interfaces;
using Checkout.Dto;
using Newtonsoft.Json;
using System;
using System.Net;

namespace Checkout.ClientLibrary.Services
{
    public class ItemService : IItemService
    {
        IClient _client;

        public ItemService(IClient client)
        {
            _client = client;
        }

        public int AddItem(int basketId, Item item)
        {
            var response = _client.AddItem(basketId, item);
            
            switch (response.StatusCode)
            {
                case HttpStatusCode.Created:
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    Item result = JsonConvert.DeserializeObject<Item>(responseString);
                    return result.ItemId;
                case HttpStatusCode.BadRequest:
                    throw new Exception("Item malformed");
                case HttpStatusCode.NotFound:
                    throw new Exception("Basket not found");
                default:
                    throw new Exception("Something went wrong with the API");
            } 
        }
         
        public Item UpdateItem(int basketId, int itemId, int quantity)
        {
            var response = _client.UpdateItemQuantity(basketId, itemId, quantity);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    Item result = JsonConvert.DeserializeObject<Item>(responseString);
                    return result;
                case HttpStatusCode.BadRequest:
                    throw new Exception("Item malformed");
                case HttpStatusCode.NotFound:
                    throw new Exception("Basket or item not found");
                default:
                    throw new Exception("Something went wrong with the API");
            }
        }

        public void DeleteItem(int basketId, int itemId)
        {
            var response = _client.RemoveItem(basketId, itemId);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    return;
                case HttpStatusCode.NotFound:
                    throw new Exception("Basket or item not found");
                default:
                    throw new Exception("Something went wrong with the API");
            } 
        } 
    }
}