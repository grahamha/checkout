﻿using Checkout.ClientLibrary.Interfaces;
using Checkout.Dto;
using Newtonsoft.Json;
using System;
using System.Net;

namespace Checkout.ClientLibrary.Services
{
    public class BasketService : IBasketService
    {
        IClient _client;

        public BasketService(IClient client)
        {
            _client = client;
        }

        public int CreateBasket()
        {
            var response = _client.CreateBasket();

            switch (response.StatusCode)
            {
                case HttpStatusCode.Created:
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    Basket result = JsonConvert.DeserializeObject<Basket>(responseString);
                    return result.BasketId;
                case HttpStatusCode.BadRequest:
                    throw new Exception("Basket malformed");
                default:
                    throw new Exception("Something went wrong with the API");
            }
        }
         
        public void ClearBasket(int basketId)
        {
            var response = _client.ClearBasket(basketId);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    return;
                case HttpStatusCode.NotFound:
                    throw new Exception("Basket not found");
                default:
                    throw new Exception("Something went wrong with the API");
            }
        }
    }
}