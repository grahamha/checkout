﻿using Checkout.ClientLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkout.ClientLibrary.Mappers
{
    public class ClassMapper : IClassMapper
    {
        public Dto.Item MapToDtoItem(Framework.Product product, int quantity)
        {
            var dtoProduct = MapToDtoProduct(product);

            return new Dto.Item
            {
                Product = dtoProduct,
                Quantity = quantity
            };
        }
         
        private Dto.Product MapToDtoProduct(Framework.Product product)
        {
            return new Dto.Product
            {
                ProductId = product.ProductId,
                UnitPrice = product.UnitPrice
            };
        }
    }
}
