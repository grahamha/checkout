﻿using Checkout.Dto;

namespace Checkout.ClientLibrary.Interfaces
{
    public interface IClassMapper
    {
        Item MapToDtoItem(Framework.Product product, int quantity);
    }
}
