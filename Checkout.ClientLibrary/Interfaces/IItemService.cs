﻿using Checkout.Dto; 

namespace Checkout.ClientLibrary.Interfaces
{
    public interface IItemService
    {
        int AddItem(int basketId, Dto.Item item);
        Item UpdateItem(int basketId, int itemId, int quantity);
        void DeleteItem(int basketId, int itemId);
    }
}
