﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkout.ClientLibrary.Interfaces
{
    public interface IBasketService
    {
        int CreateBasket();
        void ClearBasket(int basketId);
    }
}
