﻿using Checkout.Dto;
using System.Net.Http;

namespace Checkout.ClientLibrary.Interfaces
{
    public interface IClient
    {
        HttpResponseMessage AddItem(int basketId, Item item);
        HttpResponseMessage ClearBasket(int basketId);
        HttpResponseMessage CreateBasket();
        HttpResponseMessage RemoveItem(int basketId, int itemId);
        HttpResponseMessage UpdateItemQuantity(int basketId, int itemId, int quantity);
    }
}
