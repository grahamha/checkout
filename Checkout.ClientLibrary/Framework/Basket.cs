﻿using Checkout.ClientLibrary.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Checkout.ClientLibrary.Framework
{
    public class Basket
    {
        private IBasketService _basketSvc;
        private IItemService _itemSvc;
        private IClassMapper _mapper;

        public Basket(IBasketService basketSvc, IItemService itemSvc, IClassMapper mapper)
        {            
            _basketSvc = basketSvc;
            _itemSvc = itemSvc;
            _mapper = mapper;

            BasketId = _basketSvc.CreateBasket();

            Items = new List<Item>();
        }

        public int BasketId { get; private set; }
        public List<Item> Items { get; private set; }
        public decimal SubTotal
        {
            get { return Items.Sum(i => i.Price); }
        }

        public void AddProduct(Product product, int quantity)
        {
            // Call api
            var dtoItem = _mapper.MapToDtoItem(product, quantity);
            int itemId = _itemSvc.AddItem(BasketId, dtoItem);

            // Add item to list
            var item = new Item(itemId, product, quantity);
            Items.Add(item);
        }

        public void RemoveProduct(Product product)
        {
            // Check item exists
            Item item = Items.SingleOrDefault(i => i.Product.ProductId == product.ProductId);

            if (item != null)
            {
                // Call api
                _itemSvc.DeleteItem(BasketId, item.ItemId);
                
                // Remove from list
                Items.Remove(item);
            }
        }

        public void ChangeQuantity(Product product, int quantity)
        {
            // Check item exists
            Item item = Items.SingleOrDefault(i => i.Product.ProductId == product.ProductId);

            if (item != null)
            {
                // Call api
                _itemSvc.UpdateItem(BasketId, item.ItemId, quantity);

                item.UpdateQuantity(quantity);
            }
        }

        public void Clear()
        {
            // Call api
            _basketSvc.ClearBasket(BasketId);

            Items.Clear();
        }
    }
}
