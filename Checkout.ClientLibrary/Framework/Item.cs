﻿using Checkout.ClientLibrary.Interfaces;

namespace Checkout.ClientLibrary.Framework
{
    public class Item
    {
        public Item(int itemId, Product product, int quantity)
        {
            this.ItemId = itemId;
            this.Product = product;
            this.Quantity = quantity;
        }

        public int ItemId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal Price
        {
            get { return Product.UnitPrice * Quantity; }
        }

        public void UpdateQuantity(int quantity)
        {
            this.Quantity = quantity;
        }
    }
}
