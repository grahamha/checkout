﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkout.ClientLibrary.Framework
{
    public class Product
    {
        public Product(int productId, string name, decimal unitPrice)
        {
            this.ProductId = ProductId;
            this.Name = name;
            this.UnitPrice = unitPrice;
        }

        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
