﻿using Checkout.ClientLibrary.Interfaces;
using Checkout.Dto;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net.Http;
using System.Text;

namespace Checkout.ClientLibrary.Client
{
    internal class ApiClient : IClient
    {
        private HttpClient _client;

        public ApiClient()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ApiBaseAddress"]);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }

        public HttpResponseMessage AddItem(int basketId, Item item)
        {
            string json = JsonConvert.SerializeObject(item);

            return _client.PostAsync("api/baskets/" + basketId + "/items", 
                new StringContent(json, Encoding.Unicode, "application/json")).Result;            
        }

        public HttpResponseMessage ClearBasket(int basketId)
        {
            return _client.DeleteAsync("api/baskets/" + basketId + "/items").Result;
        }

        public HttpResponseMessage CreateBasket()
        {
            string json = JsonConvert.SerializeObject(new { });

            return _client.PostAsync("api/baskets", 
                new StringContent(json, Encoding.Unicode, "application/json")).Result;
        }
         
        public HttpResponseMessage RemoveItem(int basketId, int itemId)
        {
            return _client.DeleteAsync("api/baskets/" + basketId + "/items" + itemId).Result;
        }

        public HttpResponseMessage UpdateItemQuantity(int basketId, int itemId, int quantity)
        {
            string json = JsonConvert.SerializeObject(new { });

            return _client.PutAsync("api/baskets/" + basketId + "/items/" + itemId + "/quantity/" + quantity,
                new StringContent(json, Encoding.Unicode, "application/json")).Result;
        }
    }
}