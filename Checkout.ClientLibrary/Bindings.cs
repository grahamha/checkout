﻿using Checkout.ClientLibrary.Client;
using Checkout.ClientLibrary.Interfaces;
using Checkout.ClientLibrary.Mappers;
using Checkout.ClientLibrary.Services;
using Ninject.Modules;

namespace Checkout.ClientLibrary
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IBasketService>().To<BasketService>();
            Bind<IItemService>().To<ItemService>();
            Bind<IClient>().To<ApiClient>();
            Bind<IClassMapper>().To<ClassMapper>();
        }
    }
}
