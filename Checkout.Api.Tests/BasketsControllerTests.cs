﻿using Checkout.Api.Controllers;
using Checkout.Api.Repositories;
using Checkout.Dto;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace Checkout.Api.Tests
{
    [TestClass]
    public class BasketsControllerTests
    {
        private Mock<IBasketRepository> _mockBasketRepo;
        private BasketsController controller;

        [TestInitialize]
        public void SetUp()
        {
            _mockBasketRepo = new Mock<IBasketRepository>();

            controller = new BasketsController(_mockBasketRepo.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
        }
  
        [TestMethod]
        public void PostBasket_Ok()
        {
            _mockBasketRepo
                .Setup(r => r.Insert(It.IsAny<Basket>()))
                .Returns(new Basket { BasketId = 10 });               

            var response = controller.Post();

            var contentResult = response as CreatedNegotiatedContentResult<Basket>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);

            // Basket ID has been set 
            Assert.AreEqual(10, contentResult.Content.BasketId); 
        }

        [TestMethod]
        public void PostBasket_ServerError()
        { 
            _mockBasketRepo
                .Setup(r => r.Insert(It.IsAny<Basket>()))
                .Throws(new Exception());

            var response = controller.Post();
            
            var result = response as InternalServerErrorResult;

            Assert.IsNotNull(result);
        }
    }
}
