﻿using Checkout.Api.Controllers;
using Checkout.Api.Repositories;
using Checkout.Dto;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace Checkout.Api.Tests
{
    [TestClass]
    public class ItemsControllerTests
    {
        private Mock<IItemRepository> _mockItemRepo;
        private Mock<IBasketRepository> _mockBasketRepo;
        private ItemsController controller;

        [TestInitialize]
        public void SetUp()
        {
            _mockItemRepo = new Mock<IItemRepository>();
            _mockBasketRepo = new Mock<IBasketRepository>();

            controller = new ItemsController(_mockItemRepo.Object, _mockBasketRepo.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
        }
          
        [TestMethod]
        public void PostItem_Ok()
        {
            _mockBasketRepo
                .Setup(r => r.Get(It.IsAny<int>()))
                .Returns(new Basket());

            var mockItem = new Item
            {
                ItemId = 2 
            };

            _mockItemRepo
                .Setup(r => r.Insert(It.IsAny<Item>()))
                .Returns(mockItem);

            var response = controller.Post(1, mockItem);

            var contentResult = response as CreatedNegotiatedContentResult<Item>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(2, contentResult.Content.ItemId);
        }

        [TestMethod]
        public void PostItem_BadRequest()
        {
            var response = controller.Post(1, null);

            var contentResult = response as BadRequestResult;

            Assert.IsNotNull(contentResult);
        }

        [TestMethod]
        public void PostItem_BasketNotFound()
        {
            _mockBasketRepo
                .Setup(r => r.Get(1))
                .Returns<Basket>(null);

            var response = controller.Post(1, new Item());

            var contentResult = response as NotFoundResult;

            Assert.IsNotNull(contentResult);
        }

        [TestMethod]
        public void PostItem_InternalServiceError()
        {             
            _mockBasketRepo
                .Setup(r => r.Get(It.IsAny<int>()))
                .Throws(new System.Exception());

            var mockItem = new Item { ItemId = 2 };

            var response = controller.Post(1, mockItem);

            var contentResult = response as InternalServerErrorResult;

            Assert.IsNotNull(contentResult);
        }
        
        [TestMethod]
        public void PutItem_BasketNotFound()
        {
            _mockBasketRepo
                .Setup(r => r.Get(1))
                .Returns<Basket>(null);

            var response = controller.Put(1, 1, 2);

            var contentResult = response as NotFoundResult;

            Assert.IsNotNull(contentResult);
        }

        [TestMethod]
        public void PutItem_ItemNotFound()
        {
            _mockBasketRepo
                .Setup(r => r.Get(1))
                .Returns(new Basket());

            _mockItemRepo
                .Setup(r => r.Get(1, 2))
                .Returns<Item>(null);

            var response = controller.Put(1, 2, 2);

            var contentResult = response as NotFoundResult;

            Assert.IsNotNull(contentResult);
        }

        [TestMethod]
        public void PutItem_Ok()
        {
            _mockBasketRepo
                .Setup(r => r.Get(1))
                .Returns(new Basket());

            _mockItemRepo
                .Setup(r => r.Get(1, 2))
                .Returns(new Item());
 
            var response = controller.Put(1, 2, 3);

            var contentResult = response as OkNegotiatedContentResult<Item>;

            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(3, contentResult.Content.Quantity);
        }

        [TestMethod]
        public void DeleteAll_BasketNotFound()
        {
            _mockBasketRepo
                .Setup(r => r.Get(It.IsAny<int>()))
                .Returns<Basket>(null);

            var response = controller.Delete(1);

            var contentResult = response as NotFoundResult;

            Assert.IsNotNull(contentResult);
        }

        [TestMethod]
        public void DeleteAll_Ok()
        {
            _mockBasketRepo
                .Setup(r => r.Get(It.IsAny<int>()))
                .Returns(new Basket());

            _mockItemRepo
                .Setup(r => r.Get(It.IsAny<int>()))
                .Returns(new List<Item>
                {
                    new Item()
                });

            var response = controller.Delete(1);

            var contentResult = response as OkResult;

            Assert.IsNotNull(contentResult);
        }

        [TestMethod]
        public void DeleteItem_BasketNotFound()
        {
            _mockBasketRepo
                .Setup(r => r.Get(It.IsAny<int>()))
                .Returns<Basket>(null);

            var response = controller.Delete(1, 1);

            var contentResult = response as NotFoundResult;

            Assert.IsNotNull(contentResult);
        }

        [TestMethod]
        public void DeleteItem_ItemNotFound()
        {
            _mockBasketRepo
                .Setup(r => r.Get(It.IsAny<int>()))
                .Returns(new Basket());

            _mockItemRepo
                .Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns<Item>(null);

            var response = controller.Delete(1, 1);

            var contentResult = response as NotFoundResult;

            Assert.IsNotNull(contentResult);
        }

        [TestMethod]
        public void DeleteItem_Ok()
        {
            _mockBasketRepo
                .Setup(r => r.Get(It.IsAny<int>()))
                .Returns(new Basket());

            _mockItemRepo.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new Item());

            var response = controller.Delete(1, 1);

            var contentResult = response as OkResult;

            Assert.IsNotNull(contentResult);

        }
    }
}
