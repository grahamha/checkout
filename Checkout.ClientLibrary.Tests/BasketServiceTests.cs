﻿using Checkout.ClientLibrary.Interfaces;
using Checkout.ClientLibrary.Services;
using Checkout.Dto;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace Checkout.ClientLibrary.Tests
{
    [TestClass]
    public class BasketServiceTests
    {
        private Mock<IClient> _mockClient;
        private BasketService _service;

        [TestInitialize]
        public void SetUp()
        {
            _mockClient = new Mock<IClient>();
            _service = new BasketService(_mockClient.Object);
        } 

        [TestMethod]
        public void CreateBasket_Ok()
        {
            var httpResponse = new HttpResponseMessage(HttpStatusCode.Created);
            httpResponse.Content = new StringContent(JsonConvert.SerializeObject(new Basket()
            {
                BasketId = 1,
                Items = new List<Item>()
            }));

            _mockClient
                .Setup(c => c.CreateBasket())
                .Returns(httpResponse);

            int basketId = _service.CreateBasket();

            Assert.AreEqual(1, basketId);
        }

        [ExpectedException(typeof(Exception), "Basket malformed")]
        [TestMethod]
        public void CreateBasket_BadRequest()
        {
            var httpResponse = new HttpResponseMessage(HttpStatusCode.BadRequest);

            _mockClient
                .Setup(c => c.CreateBasket())
                .Returns(httpResponse);

            var result = _service.CreateBasket();            
        }

        [ExpectedException(typeof(Exception), "Something went wrong with the API")]
        [TestMethod]
        public void CreateBasket_InternalError()
        {
            var httpResponse = new HttpResponseMessage(HttpStatusCode.InternalServerError);

            _mockClient
                .Setup(c => c.CreateBasket())
                .Returns(httpResponse);

            var result = _service.CreateBasket();
        }

        [TestMethod]
        public void ClearBasket_Ok()
        {
            var httpResponse = new HttpResponseMessage(HttpStatusCode.OK);
            int mockBasketId = 1;

            _mockClient
                .Setup(c => c.ClearBasket(mockBasketId))
                .Returns(httpResponse);

            _service.ClearBasket(mockBasketId);
        }

        [ExpectedException(typeof(Exception), "Basket not found")]
        [TestMethod]
        public void ClearBasket_NotFound()
        {
            var httpResponse = new HttpResponseMessage(HttpStatusCode.NotFound);
            int mockBasketId = 1;

            _mockClient
                .Setup(c => c.ClearBasket(mockBasketId))
                .Returns(httpResponse);

            _service.ClearBasket(mockBasketId);
        }

        [ExpectedException(typeof(Exception), "Something went wrong with the API")]
        [TestMethod]
        public void ClearBasket_InternalServerError()
        {
            var httpResponse = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            int mockBasketId = 1;

            _mockClient
                .Setup(c => c.ClearBasket(mockBasketId))
                .Returns(httpResponse);

            _service.ClearBasket(mockBasketId);
        }
    }
}
