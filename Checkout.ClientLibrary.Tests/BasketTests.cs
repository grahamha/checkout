﻿using Checkout.ClientLibrary.Framework;
using Checkout.ClientLibrary.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkout.ClientLibrary.Tests
{
    [TestClass]
    public class BasketTests
    {
        private Mock<IBasketService> _mockBasketSvc;
        private Mock<IItemService> _mockItemSvc;
        private Mock<IClassMapper> _mockClassMapper;
        private Product _product;

        [TestInitialize]
        public void SetUp()
        {
            _mockBasketSvc = new Mock<IBasketService>();
            _mockItemSvc = new Mock<IItemService>();
            _mockClassMapper = new Mock<IClassMapper>(); 

            _product = new Product(1, "Cup", 1.0m);
        }

        [TestMethod]
        public void CreateBasket_HasId()
        {
            _mockBasketSvc
                .Setup(s => s.CreateBasket())
                .Returns(1);

            var basket = new Basket(_mockBasketSvc.Object, _mockItemSvc.Object, _mockClassMapper.Object);

            Assert.AreEqual(1, basket.BasketId);
        }

        [TestMethod]
        public void CreateBasket_ListNotNull()
        {
            var basket = new Basket(_mockBasketSvc.Object, _mockItemSvc.Object, _mockClassMapper.Object);
                     
            Assert.IsNotNull(basket.Items);
        }
        [TestMethod]
        public void AddProduct_IsInList()
        {
            var product = new Product(1, "Cup", 1.0m);

            var basket = new Basket(_mockBasketSvc.Object, _mockItemSvc.Object, _mockClassMapper.Object);
            basket.AddProduct(product, 1);

            Assert.IsNotNull(basket.Items.SingleOrDefault(i => i.Product.ProductId == product.ProductId));
        }

        [TestMethod]
        public void ChangeQuantity_QuantityChanged()
        {
            var basket = new Basket(_mockBasketSvc.Object, _mockItemSvc.Object, _mockClassMapper.Object);
            basket.AddProduct(_product, 1);

            basket.ChangeQuantity(_product, 2);
                        
            Assert.AreEqual(2, basket.Items.SingleOrDefault(i => i.Product.ProductId == _product.ProductId).Quantity);
        }

        [TestMethod]
        public void RemoveProduct_ProductNotInList()
        {
            var basket = new Basket(_mockBasketSvc.Object, _mockItemSvc.Object, _mockClassMapper.Object);
            basket.AddProduct(_product, 1);

            basket.RemoveProduct(_product);

            Assert.IsNull(basket.Items.SingleOrDefault(i => i.Product.ProductId == _product.ProductId));
        }

        [TestMethod]
        public void Clear_ListEmpty()
        { 
            var basket = new Basket(_mockBasketSvc.Object, _mockItemSvc.Object, _mockClassMapper.Object);
            basket.AddProduct(_product, 1);

            basket.Clear();

            Assert.AreEqual(0, basket.Items.Count);
        } 
    }
}
