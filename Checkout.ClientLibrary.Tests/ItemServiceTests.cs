﻿using Checkout.ClientLibrary.Interfaces;
using Checkout.ClientLibrary.Services;
using Checkout.Dto;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;

namespace Checkout.ClientLibrary.Tests
{
    [TestClass]
    public class ItemServiceTests
    {
        private Mock<IClient> _mockClient;
        private ItemService _service;

        [TestInitialize]
        public void SetUp()
        {
            _mockClient = new Mock<IClient>();
            _service = new ItemService(_mockClient.Object);
        }

        [TestMethod]
        public void AddItem_Ok()
        {
            int mockBasketId = 1; 
             
            var httpResponse = new HttpResponseMessage(HttpStatusCode.Created);
            httpResponse.Content = new StringContent(JsonConvert.SerializeObject(new Item
            {
                ItemId = 1,
                Product = new Product
                {
                    UnitPrice = 10
                }
            }));

            _mockClient
                .Setup(c => c.AddItem(mockBasketId, It.IsAny<Item>()))
                .Returns(httpResponse);

            var result = _service.AddItem(mockBasketId, new Item());

            Assert.AreEqual(1, result);
        }

        [ExpectedException(typeof(Exception), "Item malformed")]
        [TestMethod]
        public void AddItem_BadRequest()
        {
            int mockBasketId = 1;
            
            var httpResponse = new HttpResponseMessage(HttpStatusCode.BadRequest);

            _mockClient
                .Setup(c => c.AddItem(mockBasketId, It.IsAny<Item>()))
                .Returns(httpResponse);

            var result = _service.AddItem(mockBasketId, new Item());
        }

        [ExpectedException(typeof(Exception), "Basket not found")]
        [TestMethod]
        public void AddItem_NotFound()
        {
            int mockBasketId = 1; 
             
            var httpResponse = new HttpResponseMessage(HttpStatusCode.NotFound);

            _mockClient
                .Setup(c => c.AddItem(mockBasketId, It.IsAny<Item>()))
                .Returns(httpResponse);

            var result = _service.AddItem(mockBasketId, new Item());
        }

        [ExpectedException(typeof(Exception), "Something went wrong with the API")]
        [TestMethod]
        public void AddItem_InternalServerError()
        {
            int mockBasketId = 1;

            var httpResponse = new HttpResponseMessage(HttpStatusCode.InternalServerError);

            _mockClient
                .Setup(c => c.AddItem(mockBasketId, It.IsAny<Item>()))
                .Returns(httpResponse);

            var result = _service.AddItem(mockBasketId, new Item());
        }
         
        [TestMethod]
        public void UpdateItem_Ok()
        {
            int mockBasketId = 1;
            int mockItemId = 1;
            
            var mockItem = new Item
            {
                Quantity = 10,
                Product = new Product
                {
                    UnitPrice = 10
                }
            };

            var httpResponse = new HttpResponseMessage(HttpStatusCode.OK);
            httpResponse.Content = new StringContent(JsonConvert.SerializeObject(mockItem));

            _mockClient
                .Setup(c => c.UpdateItemQuantity(mockBasketId, mockItemId, It.IsAny<int>()))
                .Returns(httpResponse);

            var result = _service.UpdateItem(mockBasketId, mockItemId, 2);

            Assert.IsNotNull(result);
            Assert.AreEqual(10, result.Quantity);
        }

        [ExpectedException(typeof(Exception), "Item malformed")]
        [TestMethod]
        public void UpdateItem_BadRequest()
        {
            int mockBasketId = 1;
            int mockItemId = 1;

            var httpResponse = new HttpResponseMessage(HttpStatusCode.BadRequest);

            _mockClient
                .Setup(c => c.UpdateItemQuantity(mockBasketId, mockItemId, It.IsAny<int>()))
                .Returns(httpResponse);

            var result = _service.UpdateItem(mockBasketId, mockItemId, 2);
        }

        [ExpectedException(typeof(Exception), "Basket or item not found")]
        [TestMethod]
        public void UpdateItem_NotFound()
        {
            int mockBasketId = 1;
            int mockItemId = 1;
           
            var httpResponse = new HttpResponseMessage(HttpStatusCode.NotFound);

            _mockClient
                .Setup(c => c.UpdateItemQuantity(mockBasketId, mockItemId, It.IsAny<int>()))
                .Returns(httpResponse);

            var result = _service.UpdateItem(mockBasketId, mockItemId, 2);
        }

        [ExpectedException(typeof(Exception), "Something went wrong with the API")]
        [TestMethod]
        public void UpdateItem_InternalServerError()
        {
            int mockBasketId = 1;
            int mockItemId = 1;

            var httpResponse = new HttpResponseMessage(HttpStatusCode.InternalServerError);

            _mockClient
                .Setup(c => c.UpdateItemQuantity(mockBasketId, mockItemId, It.IsAny<int>()))
                .Returns(httpResponse);

            var result = _service.UpdateItem(mockBasketId, mockItemId, 2);
        }

        [TestMethod]
        public void DeleteItem_Ok()
        {
            int mockBasketId = 1;
            int mockItemId = 1;

            var httpResponse = new HttpResponseMessage(HttpStatusCode.OK);

            _mockClient
                .Setup(c => c.RemoveItem(mockBasketId, mockItemId))
                .Returns(httpResponse);

            _service.DeleteItem(mockBasketId, mockItemId);
        }

        [ExpectedException(typeof(Exception), "Basket or item not found")]
        [TestMethod]
        public void DeleteItem_BasketNotFound()
        {
            int mockBasketId = 1;
            int mockItemId = 1;

            var httpResponse = new HttpResponseMessage(HttpStatusCode.NotFound);

            _mockClient
                .Setup(c => c.RemoveItem(mockBasketId, mockItemId))
                .Returns(httpResponse);

            _service.DeleteItem(mockBasketId, mockItemId);
        }

        [ExpectedException(typeof(Exception), "Something went wrong with the API")]
        [TestMethod]
        public void DeleteItem_InternalServerError()
        {
            int mockBasketId = 1;
            int mockItemId = 1;

            var httpResponse = new HttpResponseMessage(HttpStatusCode.InternalServerError);

            _mockClient
                .Setup(c => c.RemoveItem(mockBasketId, mockItemId))
                .Returns(httpResponse);

            _service.DeleteItem(mockBasketId, mockItemId);
        }
    }
}
