# .NET Challenge #

### Overview ###

For part 1 there is a Web API project (Checkout.Api) with its associated tests (Checkout.Api.Tests).
For part 2 there is a Class Library project (Checkout.ClientLibrary) with its associated tests (Checkout.ClientLibrary.Tests).
There is a common project (Checkout.Dto) which just contains DTO used in both projects. 
 
### Assumptions ###

* As a data storage solution hasn't been provided, I have created an interface with method declarations. I mock the behaviour of these methods in the Checkout.Api.Tests projects. I have not provided a concrete version.
* As how the client library will be used hasn't been described I have assumed that it needs to provide some high level objects which can be used to manage an order of items. I have provided these in Checkout.ClientLibrary.Framework. There are services which back the operations in the high level abstraction.
* I have only provided functionality for what was described in the basic workflow, therefore there isn't functionality to retrieve a basket from the API once it has been disposed in the client library.
* I have also not added any security to the API or associated baskets with user accounts. This is because it wasn't mentioned in the basic workflow and it's a very broad topic and it seemed out of scope for this task.