﻿using Checkout.Dto;
using System.Collections.Generic;

namespace Checkout.Api.Repositories
{
    public interface IItemRepository
    {
        Item Insert(Item item);
        List<Item> Get(int basketId);
        Item Get(int basketId, int itemId);
        Item Update(Item item);
        void Delete(Item item);
    }
}
