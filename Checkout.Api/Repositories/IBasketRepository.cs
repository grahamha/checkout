﻿using Checkout.Dto;

namespace Checkout.Api.Repositories
{
    public interface IBasketRepository
    {
        Basket Insert(Basket basket);
        Basket Get(int basketId);
    }
}
