﻿using Checkout.Api.Repositories;
using Checkout.Dto;
using System;
using System.Web.Http;

namespace Checkout.Api.Controllers
{
    [RoutePrefix("api/baskets/{basketId}/items")]
    public class ItemsController : ApiController
    {
        IItemRepository _itemRepo;
        IBasketRepository _basketRepo;
         
        public ItemsController(IItemRepository itemRepo, IBasketRepository basketRepo)
        {
            _itemRepo = itemRepo;
            _basketRepo = basketRepo;
        }

        // POST: api/baskets/1/items
        [HttpPost]
        public IHttpActionResult Post(int basketId, [FromBody] Item item)
        {
            try
            {
                if (item == null)
                {
                    return BadRequest();
                }

                var basket = _basketRepo.Get(basketId);

                if (basket == null)
                {
                    return NotFound();
                }

                // Assumption - the repo returns item with ID populated 
                var result = _itemRepo.Insert(item);

                return Created(Request.RequestUri + "/" + result.ItemId.ToString(), result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }          
        }

        // PUT: api/baskets/1/items/1/quantity/1
        [Route("{itemId}/quantity/{quantity}")]
        [HttpPut]
        public IHttpActionResult Put(int basketId, int itemId, int quantity)
        { 
            try
            {
                var basket = _basketRepo.Get(basketId);

                if (basket == null)
                {
                    return NotFound();
                }

                var item = _itemRepo.Get(basketId, itemId);

                if (item == null)
                {
                    return NotFound();
                }

                item.Quantity = quantity;

                _itemRepo.Update(item);

                return Ok(item);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
            
        }

        // DELETE: api/baskets/1
        [HttpDelete]
        public IHttpActionResult Delete(int basketId)
        {
            try
            {
                var basket = _basketRepo.Get(basketId);

                if (basket == null)
                {
                    return NotFound();
                }

                var items = _itemRepo.Get(basketId);

                foreach (var item in items)
                {
                    _itemRepo.Delete(item);
                }

                return Ok();
            }
            catch (Exception)
            {
                return InternalServerError();
            }           
        }

        // DELETE: api/baskets/1/items/1
        [Route("{itemId}")]
        [HttpDelete]
        public IHttpActionResult Delete(int basketId, int itemId)
        {
            try
            {
                var basket = _basketRepo.Get(basketId);

                if (basket == null)
                {
                    return NotFound();
                }

                var item = _itemRepo.Get(basketId, itemId);

                if (item == null)
                {
                    return NotFound();
                }

                _itemRepo.Delete(item);

                return Ok();
            }
            catch (Exception)
            {
                return InternalServerError();
            }            
        }
    }     
}
