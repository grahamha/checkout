﻿using Checkout.Api.Repositories;
using Checkout.Dto;
using System;
using System.Web.Http;

namespace Checkout.Api.Controllers
{
    public class BasketsController : ApiController
    {
        private IBasketRepository _repo;
         
        public BasketsController(IBasketRepository repo)
        {
            _repo = repo;
        }
         
        // POST: api/baskets
        [HttpPost]
        public IHttpActionResult Post()
        {
            try
            {
                var basket = new Basket();

                // Assumption - the repo returns basket with ID populated
                var result = _repo.Insert(basket);

                return Created(Request.RequestUri + "/" + result.BasketId.ToString(), result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }         
        }
    }
}
